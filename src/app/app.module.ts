import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';

import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import {ToastModule} from 'ng2-toastr/ng2-toastr';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { WineListComponent } from './views/wine-list/wine-list.component';
import { WineDetailsComponent } from './views/wine-details/wine-details.component';

import { WineService } from './services/wine.service';
import { CacheService } from './services/cache.service';
import { MyCellarService } from './services/my-cellar.service';

import { WineResolver } from './resolvers/wine.resolver';
import { MyWinesComponent } from './views/my-wines/my-wines.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    WineListComponent,
    WineDetailsComponent,
    MyWinesComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpModule,
    InfiniteScrollModule,
    ToastModule.forRoot()
  ],
  providers: [WineService, CacheService, MyCellarService, WineResolver],
  bootstrap: [AppComponent]
})
export class AppModule { }
