import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WineListComponent } from './views/wine-list/wine-list.component';
import { MyWinesComponent } from './views/my-wines/my-wines.component';
import { WineDetailsComponent } from './views/wine-details/wine-details.component';
import { WineResolver } from './resolvers/wine.resolver';

const routes: Routes = [
  {
    path: '',
    component: WineListComponent,
  },
  {
    path: 'my-wines',
    component: MyWinesComponent,
  },
  {
    path: 'wine/:code',
    component: WineDetailsComponent,
    resolve: {
      wine: WineResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
