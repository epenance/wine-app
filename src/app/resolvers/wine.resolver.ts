import {Injectable} from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { WineService } from '../services/wine.service';

@Injectable()
export class WineResolver implements Resolve<any> {
  constructor(private wineService: WineService) {}
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any>|Promise<any>|any {
    return this.wineService.getWineDetails(route.params['code']);
  }
}
