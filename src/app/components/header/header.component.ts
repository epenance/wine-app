import { Component, OnInit } from '@angular/core';
import { MyCellarService } from '../../services/my-cellar.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  menuOpen = false;
  myWinesAmount: number;
  mySub: any;
  constructor(private myCellar: MyCellarService) { }

  ngOnInit() {
    this.mySub = this.myCellar.totalWines.subscribe(amount => {
      this.myWinesAmount = amount;
    });
  }

  toggleMenu() {
    this.menuOpen = !this.menuOpen;
  }


}
