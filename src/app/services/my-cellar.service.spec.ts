import { TestBed, inject } from '@angular/core/testing';
import { MyCellarService } from './my-cellar.service';

describe('MyCellarService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MyCellarService]
    });
  });

  it('should ...', inject([MyCellarService], (service: MyCellarService) => {
    expect(service).toBeTruthy();
  }));
});
