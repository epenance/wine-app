import { Injectable } from '@angular/core';

@Injectable()
export class CacheService {
  wines: any[] = [];

  constructor() { }

  setWine(code, wine) {
    this.wines[code] = wine;
  }

  getWine(code) {
    return this.wines[code];
  }

}
