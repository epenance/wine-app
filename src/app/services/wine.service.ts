import { Injectable } from '@angular/core';
import {Http, Response, URLSearchParams} from '@angular/http';
import {CacheService} from './cache.service';
import {Observable} from 'rxjs/Rx';

@Injectable()
export class WineService {
  baseUrl = 'http://api.snooth.com';
  apiKey = '2phskcanxg8ejo3r1w1s1nlda7g3yqqqsf147821g285od46';
  currentWineList = [];

  /**
   * Winelist params
   */
  currentOffset = 1;
  limit = 9;

  constructor(private http: Http, private cache: CacheService) {
  }

  getWineDetails(code: string) {
    const params: URLSearchParams = new URLSearchParams();
    params.set('akey', this.apiKey);
    params.set('id', code);

    if (!this.cache.getWine(code)) {
      return this.http.get(`${this.baseUrl}/wine/`, {
        search: params
      })
        .map((res: Response) => {
        if (res.json().wines.length) {
          this.cache.setWine(code,  res.json().wines[0]);
          return res.json().wines[0];
        } else {
          return false;
        }
      })
        .catch((error) => {
          return Observable.throw(error);
        });
    } else {
      return Observable.of(this.cache.getWine(code));
    }
  }

  fetchWines() {
    const params: URLSearchParams = new URLSearchParams();
    /**
     * Params:
     * @f - Offset
     * @n - Limit
     * @a - Available (0 = all wines, 1 = only wines in stock)
     * @t - Product type ('wines', 'spirits', 'table, 'desert','sparkling', 'fruit',
     * 'cider', 'sake'. (If blank includes both wines and spirits)
     * @color - Color of the product ('red', 'white', 'rose', 'amber', 'clear')
     * @mp - Minimum price in dollars
     * @xp - Maximum price in dollars
     * @mr - Minimum rating
     * @xr - Maximum rating
     */
    params.set('akey', this.apiKey);
    params.set('f', (this.currentWineList.length + 1).toString());
    params.set('n', this.limit.toString());

    return this.http.get(`${this.baseUrl}/wines/`, {
      search: params
    }).map((res: Response) => {
      res.json().wines.map(wine => {
        this.currentWineList.push(wine);
      });

      return this.currentWineList;
    }).catch((error) => {
      return Observable.throw(error);
    });
  }

  getWines() {
    if (this.currentWineList.length) {
      return Observable.of(this.currentWineList);
    } else {
      return this.fetchWines();
    }

  }

}
