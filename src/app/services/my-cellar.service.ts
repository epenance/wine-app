import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Injectable()
export class MyCellarService {
  myWines: any = {};
  totalWines: BehaviorSubject<number>;
  getWines: BehaviorSubject<any[]>;

  constructor(private toastr: ToastsManager) {
    const wines = JSON.parse(localStorage.getItem('wines'));

    if (wines) {
      Object.keys(wines).map((code) => {
       this.myWines[code] = wines[code];
      });
    }

    this.totalWines = new BehaviorSubject(Object.keys(this.myWines).length);

    const parsedWines = [];

    Object.keys(this.myWines).map(wine => {
      parsedWines.push(this.myWines[wine]);
    });

    this.getWines = new BehaviorSubject(parsedWines);
  }


  addWine(wine) {
    if (typeof this.myWines[wine.code] !== 'undefined') {
      this.toastr.warning('Wine already exists in your cellar', 'Woops!');
    } else {
      this.myWines[wine.code] = wine;
      localStorage.setItem('wines', JSON.stringify(this.myWines));
      this.totalWines.next(Object.keys(this.myWines).length);
      this.toastr.success('The wine was added to your cellar, happy drinking!', 'Wine added');

      const parsedWines = [];

      Object.keys(this.myWines).map(parsedWine => {
        parsedWines.push(this.myWines[parsedWine]);
      });

      this.getWines.next(parsedWines);
    }
  }

  removeWine(wine) {
    if (typeof this.myWines[wine.code] === 'undefined') {
      this.toastr.warning('Wine was already removed', 'Woops!');
    } else {
      delete this.myWines[wine.code];
      localStorage.setItem('wines', JSON.stringify(this.myWines));
      this.totalWines.next(Object.keys(this.myWines).length);
      this.toastr.success(`The wine was removed from your cellar, but don't worry you have ${Object.keys(this.myWines).length} left! :-)`, 'Wine added');

      const parsedWines = [];

      Object.keys(this.myWines).map(parsedWine => {
        parsedWines.push(this.myWines[parsedWine]);
      });

      this.getWines.next(parsedWines);
    }
  }


  /*getWines() {
    const wines = [];

    Object.keys(this.myWines).map(wine => {
      wines.push(this.myWines[wine]);
    });

    return Observable.of(wines);
  }
  */

  checkIfOwned(code) {
    return typeof this.myWines[code] !== 'undefined';
  }

}
