import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MyCellarService } from '../../services/my-cellar.service';

@Component({
  selector: 'app-wine-details',
  templateUrl: './wine-details.component.html',
  styleUrls: ['./wine-details.component.scss']
})
export class WineDetailsComponent implements OnInit {
  wine: any;
  showTab = 'content';

  constructor(private route: ActivatedRoute, private myCellar: MyCellarService) { }

  ngOnInit() {
    this.route.data.subscribe(() => {
      this.wine = this.route.snapshot.data['wine'];
    });
  }

  toggleTab(tab) {
    this.showTab = tab;
  }

  addWineToCellar(wine) {
    this.myCellar.addWine(wine);
  }

  removeWineFromCellar(wine) {
    this.myCellar.removeWine(wine);
  }

  alreadyOwned(wine) {
    return this.myCellar.checkIfOwned(wine.code);
  }
}
