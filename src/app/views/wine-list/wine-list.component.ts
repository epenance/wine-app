import { Component, OnInit, OnDestroy } from '@angular/core';
import { WineService } from '../../services/wine.service';
import { MyCellarService } from '../../services/my-cellar.service';


@Component({
  selector: 'app-wine-list',
  templateUrl: './wine-list.component.html',
  styleUrls: ['./wine-list.component.scss']
})
export class WineListComponent implements OnInit, OnDestroy {
  wines: any[];
  showSpinner = true;
  myWines: any[];
  winesSub: any;
  myWinesSub: any;


  constructor(private wineService: WineService, private myCellar: MyCellarService) {

  }

  ngOnInit() {
    this.winesSub = this.wineService.getWines().subscribe(wines => {
      this.wines = wines;
      this.showSpinner = false;
    });

    this.myWinesSub = this.myCellar.getWines.subscribe(wines => {
      this.myWines = wines;
    });
  }

  ngOnDestroy() {
    this.winesSub.unsubscribe();
    this.myWinesSub.unsubscribe();
  }

  onScroll () {
    this.showSpinner = true;
    this.wineService.fetchWines().subscribe(wines => {
      this.wines = wines;
      this.showSpinner = false;
    });
  }

  alreadyOwned(wine) {
    return this.myCellar.checkIfOwned(wine.code);
  }

  addWineToCellar(wine) {
    this.myCellar.addWine(wine);
  }

  removeWineFromCellar(wine) {
    this.myCellar.removeWine(wine);
  }
}
