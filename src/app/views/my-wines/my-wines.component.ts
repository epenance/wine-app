import { Component, OnInit, OnDestroy } from '@angular/core';
import { MyCellarService } from '../../services/my-cellar.service';

@Component({
  selector: 'app-my-wines',
  templateUrl: './my-wines.component.html',
  styleUrls: ['./my-wines.component.scss']
})
export class MyWinesComponent implements OnInit, OnDestroy {
  wines: any[];
  showSpinner = true;
  winesSub: any;

  constructor(private myCellar: MyCellarService) { }

  ngOnInit() {
    this.winesSub = this.myCellar.getWines.subscribe(wines => {
      this.wines = wines;
    });
  }

  ngOnDestroy() {
    this.winesSub.unsubscribe();
  }

  removeWineFromCellar(wine) {
    this.myCellar.removeWine(wine);
  }

}
