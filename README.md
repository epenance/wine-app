# WineApp

This is a small project showcasing the use of Snooth's API together with Angular and Localstorage.

The app has been split up into 3 services.

### Wine Service
This is the service that implements the Snooth API and makes sure to get the list of wines as well as the detailed view of the wine upon user interaction.

### My Cellar Service
This is the service that is used to store the users wines into the Localstorage of the device, this should of course be a database if the app was to launch into a real life app.

### Cache service
Upon calls of the detail view for a wine, we cache the wine the very first time we hit it, so that we dont need to make any additional calls were we to return later on.

This is implemented to make sure the API is not overused, not giving it any additional load, or using up your quota too fast if the API was to be limited. 

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.